<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp"%>
<title>Crimsonfile - Login</title>
<link rel="stylesheet" href="../../css/bootstrap.min.css">
<link rel="stylesheet" href="../../css/login.css">
<script src="../../js/login.js"></script>
</head>
<body>
	<div class='header'>
		Crimsonfile / Login
	</div>
	<div class='box'>
		<div class='box-form'>
			<div class='box-login-tab'></div>
			<div class='box-login-title'>
				<h2>Crimsonfile</h2>
			</div>
			<div class='box-login'>
				<div class='fieldset-body' id='login_form'>
					<button onclick="openLoginInfo();" class='b b-form i i-more'
						title='Mais Informações'></button>
					<form:form modelAttribute="loginDto" action="/login" method="POST">
						<p class='field'>
							<label for='email'>ID</label> <input type='text' id='email'
								name='email' title='Email' value="test@test" /> <span id='valida'
								class='i i-warning'></span>
						</p>
						<p class='field'>
							<label for='password'>Password</label> <input type='password'
								id='password' name='password' title='Password' value="test" /> <span
								id='valida' class='i i-close'></span>
						</p>

						<label class='checkbox'> <input type='checkbox'
							value='TRUE' title='Keep me Signed in' /> Save password
						</label>
						<input type='submit' id='do_login' value='Login' title='login' />
					</form:form>
				</div>
			</div>
		</div>
		<div class='box-info'>
			<p>
				<button onclick="closeLoginInfo();" class='b b-info i i-left'
					title='Back to Sign In'></button>
			<h3>Information</h3>
			</p>
			<div class='line-wh'></div>
			<button onclick="" class='b-support' title='Forgot Password?'>
				Forget Password</button>
			<button onclick="" class='b-support' title='Contact Support'>
				Contact</button>
			<div class='line-wh'></div>
			<a href="/user/signup"><button onclick="" class='b-cta'
					title='Sign up now!'>CREATE ACCOUNT</button></a>
		</div>
	</div>
	<%@ include file="../common/footer.jsp"%>
</body>
</html>