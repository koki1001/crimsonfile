<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="../../common/common.jsp"%>
<%@ page import="java.util.Date, java.text.DateFormat" %>
<title>Crimsonfile - Diary</title>
<link rel="stylesheet" href="../../../css/bootstrap.min.css">
<link rel="stylesheet" href="../../../css/diary.css">
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/redmond/jquery-ui.css" >
<script src="../../../js/diary.js"></script>
</head>
<body>
	<a name="top"></a>
	<div class='header'>Crimsonfile / Diary</div>
	<div class='box'>
	<div class='box_edit'>
		<div>
			<a href="${pageContext.request.contextPath}/crimsonfile/"> <label
				style="margin-right: 10px; border: 1px solid #ddd; padding: 10px; cursor: pointer; border-radius: 5px; color: #666; background: linear-gradient(to bottom, #fff 0, #f4f4f4 100%); box-shadow: inset 0px -1px 0px 0px rgba(0, 0, 0, 0.09);"><span class="glyphicon glyphicon-chevron-left"></span> Back
					<input type="button" style="display: none;">
			</label></a>
			 <a href="${pageContext.request.contextPath}/crimsonfile/diary/regist">
				<label
				style="margin-right: 10px; border: 1px solid #ddd; padding: 10px; cursor: pointer; border-radius: 5px; color: #666; background: linear-gradient(to bottom, #fff 0, #f4f4f4 100%); box-shadow: inset 0px -1px 0px 0px rgba(0, 0, 0, 0.09);"><span class="glyphicon glyphicon-edit"></span> New diary<input type="button" style="display: none;">
			</label>
			</a>
			<a href="${pageContext.request.contextPath}/crimsonfile/diary/">
				<label
				style="float: right; border: 1px solid #ddd; padding: 10px; cursor: pointer; border-radius: 5px; color: #666; background: linear-gradient(to bottom, #fff 0, #f4f4f4 100%); box-shadow: inset 0px -1px 0px 0px rgba(0, 0, 0, 0.09);"><span class="glyphicon glyphicon-refresh"></span> Refresh<input type="button" style="display: none;">
			</label>
			</a>
		</div>
		<div class="search">
			<div class="date" style="float: left;">
				<form:form modelAttribute="searchDto" action="/crimsonfile/diary/searchByDate" method="POST" id="form4" role="form">
					<input id="datepicker" name="date" type="text" value="${date}" placeholder="Input date">
					<button id="sbtn4" type="submit"><i style="color:#fff;margin-right: 3px;" class="glyphicon glyphicon-calendar"></i></button>
				</form:form>
			</div>
			<div class="page" style="float: right;">
				<form:form modelAttribute="searchDto" action="/crimsonfile/diary/searchByPage" method="POST" id="form4" role="form">
					<input id="sbox4" name="page" type="text" placeholder="Input page number" />
					<button id="sbtn4" type="submit"><i style="color:#fff;margin-right: 3px;" class="glyphicon glyphicon-search"></i></button>
				</form:form>
			</div>
			<div class="text" style="float: right;margin-right: 230px;">
				<form:form modelAttribute="searchDto" action="/crimsonfile/diary/searchByContent" method="POST" id="form4" role="form">
					<input id="sbox4" name="content" type="text" placeholder="Input title or content" />
					<button id="sbtn4" type="submit"><i style="color:#fff;margin-right: 3px;" class="glyphicon glyphicon-search"></i></button>
				</form:form>
			</div>
		</div>
		<div class="text-right favorite">
			<a href="${pageContext.request.contextPath}/crimsonfile/diary/displayfavorite/">Display only favorite diary</a>
		</div>
		<div class="content">
			<table class="table table-hover" style="table-layout:fixed;width:100%;">
				<thead>
					<tr>
						<th scope="col" width="160px">Date</th>
						<th scope="col" width="240px">Title</th>
						<th scope="col" width="480px">Content</th>
						<th scope="col">Action</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="item" items="${diaryEntityList}">
						<tr class="tr">
							<th scope="row" style="padding: 12px 8px;"><c:out value="${item.dateTime}" /></th>
							<td style="padding: 12px 8px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;"><c:out value="${item.title}" /></td>
							<td style="padding: 12px 8px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;"><c:out value="${item.content}" /></td>
							<td style="padding: 12px 8px;">
							<a href="/crimsonfile/diary/edit?diaryId=${item.diaryId}"><span class="glyphicon glyphicon-pencil" style="padding-right:16px;"></span></a>
							<a href="/crimsonfile/diary/view?diaryId=${item.diaryId}"><span class="glyphicon glyphicon-book" style="padding-right:16px;"></span></a>
							<a href="/crimsonfile/diary/delete?diaryId=${item.diaryId}" onClick="if(!confirm('Are you sure you want to delete this?'))return false"><span class="glyphicon glyphicon-erase" style="padding-right:16px;"></span></a>
							<c:if test="${item.favoriteFlag == 0}">
								<a href="/crimsonfile/diary/favorite?diaryId=${item.diaryId}"><span class="glyphicon glyphicon-star-empty" style=""></span></a>
							</c:if>
							<c:if test="${item.favoriteFlag == 1}">
								<a href="/crimsonfile/diary/favoriteempty?diaryId=${item.diaryId}"><span class="glyphicon glyphicon-star" style=""></span></a>
							</c:if>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
		<div class="text-right">
			<a href="#top"> <label
				style="border: 1px solid #ddd; padding: 10px; cursor: pointer; border-radius: 5px; color: #666; background: linear-gradient(to bottom, #fff 0, #f4f4f4 100%); box-shadow: inset 0px -1px 0px 0px rgba(0, 0, 0, 0.09);"><span class="glyphicon glyphicon-arrow-up"></span> Top
					<input type="button" style="display: none;">
			</label>
			</a>
		</div>
		<div class="paging">
			<div style="float: left; width: 70px;">
				<h3 style="font-size:16px;">
					<c:if test="${pageNumber == 1}">
						<a href="/crimsonfile/diary/"><span class="glyphicon glyphicon-arrow-left"></span> Prev</a>
					</c:if>
					<c:if test="${pageNumber > 1}">
						<a href="/crimsonfile/diary/page?num=${pageNumber-1}"><span class="glyphicon glyphicon-arrow-left"></span> Prev</a>
					</c:if>
				</h3>
			</div>
			<div style="float: left; width: 920px;font-size:16px;">
				<h3>
					<c:if test="${pageNumber < pageSum}">
						<c:out value="${pageNumber}"></c:out> / <c:out value="${pageSum}"></c:out>
					</c:if>
					<c:if test="${pageNumber >= pageSum}">
						Max page number is <c:out value="${pageSum}"></c:out>
					</c:if>
				</h3>
			</div>
			<div style="float: left; width: 70px;">
				<h3 style="font-size:16px;">
					<c:if test="${pageNumber < pageSum}">
						<a href="/crimsonfile/diary/page?num=${pageNumber+1}">Next <span class="glyphicon glyphicon-arrow-right"></span></a>
					</c:if>
					<c:if test="${pageNumber >= pageSum}">
						<a href="/crimsonfile/diary/page?num=${pageSum}">Next <span class="glyphicon glyphicon-arrow-right"></span></a>
					</c:if>
				</h3>
			</div>
		</div>
		</div>
	</div>

	<%@ include file="../../common/footer.jsp"%>
</body>
</html>