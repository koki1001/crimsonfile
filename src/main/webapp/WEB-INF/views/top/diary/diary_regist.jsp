<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="../../common/common.jsp"%>
<title>Crimsonfile - Diary</title>
<link rel="stylesheet" href="../../../css/bootstrap.min.css">
<link rel="stylesheet" href="../../../css/diary.css">
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/redmond/jquery-ui.css" >
<script src="../../../js/diary.js"></script>
</head>
<body>
	<div class='header'>Crimsonfile / Diary</div>
	<div class='container-fluid box'>
	<div class='box_edit'>
		<div style="margin-bottom: 40px;">
			<a href="${pageContext.request.contextPath}/crimsonfile/diary/">
				<label
				style="margin-right: 10px; border: 1px solid #ddd; padding: 10px; cursor: pointer; border-radius: 5px; color: #666; background: linear-gradient(to bottom, #fff 0, #f4f4f4 100%); box-shadow: inset 0px -1px 0px 0px rgba(0, 0, 0, 0.09);"><span class="glyphicon glyphicon-chevron-left"></span> Back
					<input type="button" style="display: none;">
			</label>
			</a> <a
				href="${pageContext.request.contextPath}/crimsonfile/diary/regist">
				<label
				style="margin-right: 10px; border: 1px solid #ddd; padding: 10px; cursor: pointer; border-radius: 5px; color: #666; background: linear-gradient(to bottom, #fff 0, #f4f4f4 100%); box-shadow: inset 0px -1px 0px 0px rgba(0, 0, 0, 0.09);"><span class="glyphicon glyphicon-repeat"></span> Reload
					<input type="button" style="display: none;">
			</label>
			</a>
		</div>
		<div class="text-center" style="padding-bottom: 20px;">
			<h1>Write your something here...</h1>
		</div>
		<div style="width: 80%;margin: 0px auto;">
		<form:form modelAttribute="diaryDto"
			action="/crimsonfile/diary/registcomplete" class="form-horizontal"
			method="POST" id="diaryDto" role="form">
			<div class="form-group">
				<input type="text" name="date" id="datepicker" class="form-control"
					placeholder="Date" value="${date}"><label for="date"></label><br>
			</div>
			<div class="form-group">
				<input type="text" name="title" id="title" class="form-control"
					placeholder="Title"><label for="title"></label><br>
			</div>
			<div class="form-group">
				<textarea name="content" id="content" class="form-control" rows="20"
					placeholder="Content"></textarea>
				<label for="content"></label>
			</div>
			<div class="form-group">
				<button id="button-confirm" type="submit" class='b-cta form-control'>REGIST</button>
			</div>
		</form:form>
		</div>
		</div>
	</div>

	<%@ include file="../../common/footer.jsp"%>
</body>
</html>