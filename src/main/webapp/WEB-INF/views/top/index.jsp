<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp"%>
<title>Crimsonfile - Top</title>
<link rel="stylesheet" href="../../css/bootstrap.min.css">
<link rel="stylesheet" href="../../css/index.css">
<script src="../../js/index.js"></script>
<script src="../../js/transition.js"></script>
<script src="../../js/logout.js"></script>
<script></script>
</head>
<body>
	<div class='header'>Crimsonfile</div>
	<div class="logout">
		<a onClick="dialog()" style="font-size: 18px; margin-right: 5px;">Logout</a>
	</div>
	<div class="main-content container-fluid">
		<div class="main">
		<div class="sub-content row">
			<div class='profile'>
				<div class='profile-tab'></div>
				<div class='profile-title'>
					<h1>Profile</h1>
				</div>
				<div class='profile-content'>
					<div class='fieldset-body-profile'>
						<p>
							<img class="img-circle" src="${user.profilePhoto}" width="160px"
								height="160px" alt="No registered" style="margin-bottom: 16px;">
						</p>
						<p>
							Name：
							<c:out value="${user.name}" />
						</p>
						<p>
							Sex：
							<c:out value="${user.gender}" />
						</p>
						<p>
							Birth：
							<c:out value="${user.birth}" />
						</p>
						<p>
							Blood Type：
							<c:out value="${user.bloodType}" />
						</p>
						<p>
							Family Register：
							<c:out value="${user.familyRegister}" />
						</p>
						<p>
							Marital Status：
							<c:out value="${user.maritalStatus}" />
						</p>
						<div class="settings">
							<a data-toggle="modal" data-target="#sampleModal"><img
								src="../../img/setting.png" width="25px" height="25px"
								data-toggle="tooltip" data-placement="top"
								data-original-title="edit"></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="content">
			<div class='box' onClick="goDiary()">
				<div class='box-tab'></div>
				<div class='box-title'>
					<h2>Diary</h2>
				</div>
				<div class='box-content'>
					<div class='fieldset-body' id='login_form'>
						<div class="site-logo">
							<div class="site-info">
								<h1>Diary</h1>
								<p>Record everyday.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class='box'>
				<div class='box-tab'></div>
				<div class='box-title'>
					<h2>Schedule</h2>
				</div>
				<div class='box-content'>
					<div class='fieldset-body' id='login_form'>
						<div class="site-logo">
							<div class="site-info">
								<h1>Schedule</h1>
								<p>Check the day.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class='box'>
				<div class='box-tab'></div>
				<div class='box-title'>
					<h2>Work</h2>
				</div>
				<div class='box-content'>
					<div class='fieldset-body' id='login_form'>
						<div class="site-logo">
							<div class="site-info">
								<h1>Work</h1>
								<p>Support business.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<br>
			<div class='box'>
				<div class='box-tab'></div>
				<div class='box-title'>
					<h2>History</h2>
				</div>
				<div class='box-content'>
					<div class='fieldset-body' id='login_form'>
						<div class="site-logo">
							<div class="site-info">
								<h1>History</h1>
								<p>Confirm career.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class='box'>
				<div class='box-tab'></div>
				<div class='box-title'>
					<h2>Money</h2>
				</div>
				<div class='box-content'>
					<div class='fieldset-body' id='login_form'>
						<div class="site-logo">
							<div class="site-info">
								<h1>Money</h1>
								<p>Manage assets.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class='box'>
				<div class='box-tab'></div>
				<div class='box-title'>
					<h2>Memo</h2>
				</div>
				<div class='box-content'>
					<div class='fieldset-body' id='login_form'>
						<div class="site-logo">
							<div class="site-info">
								<h1>Memo</h1>
								<p>Write & Read memo.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			</div>
		</div>
	</div>
	<!-- モーダル・ダイアログ -->
	<div class="modal fade" id="sampleModal" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span>×</span>
					</button>
					<h4 class="modal-title">Profile Edit</h4>
				</div>
				<form:form modelAttribute="signUpDto" action="/user/edit"
					method="POST" id="signUpDto" class="form-horizontal" role="form"
					enctype="multipart/form-data">
					<div class="modal-body">
						<div class="form-group">
							<label for="name">Name<span class="ate1"
								style="margin-left: 10px;">Required</span></label><input type="text"
								name="name" id="name" class="form-control" value="${user.name}"
								placeholder="Please input 20 characters or less">
						</div>
						<div class="form-group">
							<label for="gender">Sex<span class="ate1"
								style="margin-left: 10px;">Required</span></label><br>
							<c:if test="${user.gender == 'Man'}">
								<input type="radio" name="gender" value="Man" id="gender"
									checked>Man
								<input type="radio" name="gender" value="Woman" id="gender">Woman
							</c:if>
							<c:if test="${user.gender == 'Woman'}">
								<input type="radio" name="gender" value="Man" id="gender">Man
								<input type="radio" name="gender" value="Woman" id="gender"
									checked>Woman
							</c:if>
						</div>
						<div class="form-group">
							<label for="familyRegister">Family Register<span
								class="ate1" style="margin-left: 10px;">Required</span></label><input
								type="text" name="familyRegister" id="familyRegister"
								class="form-control" value="${user.familyRegister}"
								placeholder="Please input 20 characters or less">
						</div>
						<div class="form-group">
							<label for="maritalStatus">Material Status<span
								class="ate1" style="margin-left: 10px;">Required</span></label><br>
							<c:if test="${user.maritalStatus == 'Not Exists'}">
								<input type="radio" name="maritalStatus" value="Not Exists"
									id="maritalStatus" checked>Not Exists
								<input type="radio" name="maritalStatus" value="Exists"
									id="maritalStatus">Exists
							</c:if>
							<c:if test="${user.maritalStatus == 'Exists'}">
								<input type="radio" name="maritalStatus" value="Not Exists"
									id="maritalStatus">Not Exists
							<input type="radio" name="maritalStatus" value="Exists"
									id="maritalStatus" checked>Exists
							</c:if>
							<c:if test="${user.maritalStatus == null}">
								<input type="radio" name="maritalStatus" value="Not Exists"
									id="maritalStatus" checked>Not Exists
							<input type="radio" name="maritalStatus" value="Exists"
									id="maritalStatus">Exists
							</c:if>
						</div>
						<!--
						<div class="form-group">
							<label for="email">Email</label><input type="text" name="email"
								id="email" class="form-control" value="${user.email}"
								placeholder="Email">
							<c:out value="${msg}" />
						</div>
						<div class="form-group">
							<label for="confirmationPassword">パスワードの確認</label><input
								type="password" name="confirmationPassword"
								id="confirmationPassword" class="form-control"
								placeholder="Confirmation Password">
						</div>
						 -->
						<div class="form-group">
							<label for="profilePhoto">Profile Photo<span class="ate2"
								style="margin-left: 10px;">Optional</span></label><br> <label
								style="width: 86px; margin-top: 5px; border: 1px solid #ddd; padding: 10px; cursor: pointer; border-radius: 5px; color: #666; background: linear-gradient(to bottom, #fff 0, #f4f4f4 100%); box-shadow: inset 0px -1px 0px 0px rgba(0, 0, 0, 0.09);">Select
								File <input type="file" name="profilePhoto" id="profilePhoto"
								value="${user.profilePhoto}" style="display: none;">
							</label>
						</div>
						<div class="form-group">
							<label for="password">New Password<span class="ate2"
								style="margin-left: 10px;">Optional</span></label><input type="password"
								name="password" id="password" class="form-control"
								placeholder="Please input new password">
						</div>
						<div class="form-group">
							<label for="confirmationPassword">Confirmation Password<span
								class="ate2" style="margin-left: 10px;">Optional</span></label><input
								type="password" name="confirmationPassword"
								id="confirmationPassword" class="form-control"
								placeholder="Please input new password one more time">
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button id="button-confirm" type="submit" class="btn edit-btn">Edit</button>
					</div>
				</form:form>
			</div>
		</div>
	</div>
	<div class='icon-credits'>
		<a>previous login: <fmt:formatDate value="${user.previousLogin}"
				pattern="yyyy/MM/dd HH:mm:ss" /></a>
	</div>
</body>
</html>