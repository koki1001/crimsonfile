<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp"%>
<title>Crimsonfile - Top</title>
<link rel="stylesheet" href="../../css/bootstrap.min.css">
<link rel="stylesheet" href="../../../css/bootstrap.min.css">
<link rel="stylesheet" href="../../../css/diary.css">
</head>
<body>
	<div class='header'>Crimsonfile / Error</div>
	<div class="text-center" style="margin-top:120px;">
	<h2 style="color:white;">
		System error has occurred.
		Please check again at a time.
	</h2>
	<h3 >
		<a href="${pageContext.request.contextPath}/crimsonfile/" style="text-decoration:none;">
			return to top page
		</a>
	</h3>
	</div>
</body>
</html>