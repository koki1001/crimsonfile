/*!
 * Copyright:
 * URL: https://
 * A PEN BY:
 *
 * クリエイトアカウントフォーム用.
 */
$(function() {
    $('#signUpDto').bootstrapValidator({
    	live: 'submitted',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                validators: {
                    notEmpty: { message: ' ' }
                }
            },
        	familyRegister: {
        		validators: {
        			notEmpty: { message: ' ' }
        		}
        	},
        	email: {
        		validators: {
        			notEmpty: { message: ' ' }
        		}
        	},
        	password: {
        		validators: {
        			notEmpty: { message: ' ' }
        		}
        	},
        	confirmationPassword: {
        		validators: {
        			notEmpty: { message: ' ' }
        		}
        	},
        },
    });
    $('#button-confirm').click(function() {
        $('#signUpDto').bootstrapValidator('validate');
    });
});