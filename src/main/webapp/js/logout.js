/*
 * @author koki.esaki
 *
 * ログアウト用.
 */
function dialog() {
	// 「OK」時の処理開始 ＋ 確認ダイアログの表示
	if (window.confirm('Are you sure you want to logout?')) {
		if (location.href == "http://localhost:8080/user/edit" || location.href == "http://localhost:8080/crimsonfile/") {
			location.href = "/user/login";
		}
	}
}