$(function() {
    $('#diaryDto').bootstrapValidator({
    	live: 'submitted',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            title: {
                validators: {
                    notEmpty: { message: ' ' }
                }
            },
        	content: {
        		validators: {
        			notEmpty: { message: ' ' }
        		}
        	},
        },
    });
    $('#button-confirm').click(function() {
        $('#diaryDto').bootstrapValidator('validate');
    });
});

$(function() {
    $("#datepicker").datepicker();
});