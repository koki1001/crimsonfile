package com.application.crimsonfile.logic;

import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.application.crimsonfile.dao.UserDao;
import com.application.crimsonfile.dto.SignUpDto;
import com.application.crimsonfile.entity.UserEntity;

import lombok.NonNull;

/**
 * @author koki.esaki
 *
 */
@Component
public class UserLogic {

    @Autowired
    UserDao userDao;

    @Autowired
    HttpSession session;

    @Autowired
    PasswordEncoder encoder;

    /**
     * ユーザ情報の登録.
     *
     * @param user
     * @return UserEntity
     * @throws IOException
     * @throws IllegalStateException
     */
    public void saveUserInfo(@NonNull final SignUpDto signUpDto) throws IllegalStateException, IOException {

        UserEntity user = new UserEntity();
        user.setName(signUpDto.getName());
        user.setGender(signUpDto.getGender());
        user.setMaritalStatus(signUpDto.getMaritalStatus());
        user.setBirth(signUpDto.getYear() + signUpDto.getMonth() + signUpDto.getDay());
        user.setBloodType(signUpDto.getBloodType());
        user.setFamilyRegister(signUpDto.getFamilyRegister());
        user.setEmail(signUpDto.getEmail());
        user.setPassword(encoder.encode(signUpDto.getPassword()));
        try {
            user.setProfilePhoto("data:image/jpeg;base64," + upload(signUpDto.getProfilePhoto()));
        } catch (Exception e) {
            e.getMessage();
        }
        user.setAuthority(0);

        userDao.save(user);
    }

    /**
     * ユーザー情報を編集.
     *
     * @return
     * @throws IOException
     * @throws IllegalStateException
     */
    public void editUserInfo(@NonNull final SignUpDto signUpDto) throws IllegalStateException, IOException {

        UserEntity user = (UserEntity) session.getAttribute("user");
        if (user != null) {
            user.setName(signUpDto.getName());
            user.setGender(signUpDto.getGender());
            user.setFamilyRegister(signUpDto.getFamilyRegister());
            user.setMaritalStatus(signUpDto.getMaritalStatus());
            if (!StringUtils.isEmpty(signUpDto.getPassword())) {
                if (signUpDto.getPassword().equals(signUpDto.getConfirmationPassword())) {
                    user.setPassword(encoder.encode(signUpDto.getPassword()));
                }
            }
            try {
                user.setProfilePhoto("data:image/jpeg;base64," + upload(signUpDto.getProfilePhoto()));
            } catch (Exception e) {
                e.getMessage();
            } finally {
                userDao.save(user);
                session.setAttribute("user", user);
            }
        }
    }

    /**
     * プロフィール写真のアップロード
     *
     * @param profilePhoto
     * @return
     * @throws IllegalStateException
     * @throws IOException
     */
    private String upload(final @NonNull MultipartFile profilePhoto) throws IllegalStateException, IOException {
        MultipartFile multipartFile = profilePhoto;
        multipartFile.transferTo(new File(System.getProperty("user.dir") + "\\src\\main\\webapp\\img\\", multipartFile.getOriginalFilename()));
        File file = new File(System.getProperty("user.dir") + "\\src\\main\\webapp\\img\\", multipartFile.getOriginalFilename());
        BufferedImage mainImage = null;
        mainImage = ImageIO.read(file);
        // BufferedImage subImage=mainImage.getSubimage(0,0,400,400);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        BufferedOutputStream bos = new BufferedOutputStream(baos);
        mainImage.flush();
        ImageIO.write(mainImage, "jpg", bos);
        bos.flush();
        bos.close();
        byte[] bImage = baos.toByteArray();
        return net.arnx.jsonic.util.Base64.encode(bImage);
    }

}
