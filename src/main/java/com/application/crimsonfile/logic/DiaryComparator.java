package com.application.crimsonfile.logic;

import java.util.Comparator;

import com.application.crimsonfile.entity.DiaryEntity;

/**
 * @author koki.esaki
 *
 */
public class DiaryComparator implements Comparator<DiaryEntity> {

    @Override
    public int compare(DiaryEntity d1, DiaryEntity d2) {
        return d1.getDiaryId() > d2.getDiaryId() ? -1 : 1;
    }
}
