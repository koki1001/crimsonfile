package com.application.crimsonfile.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

/**
 * @author koki.esaki
 *
 */
@Data
@Entity
@Table(name = "diary")
public class DiaryEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "diaryid_seq")
    @SequenceGenerator(name = "diaryid_seq", sequenceName = "diaryid_seq", allocationSize = 1)
    private int diaryId;

    @Column(name = "user_id")
    private int userId;

    @Column(name = "create_date_time")
    private String createDateTime;

    @Column(name = "title")
    private String title;

    @Column(name = "content")
    private String content;

    @Column(name = "date_time")
    private String dateTime;

    @Column(name = "favorite_flag")
    private String favoriteFlag;

}
