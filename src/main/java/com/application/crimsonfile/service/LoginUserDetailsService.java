package com.application.crimsonfile.service;

import java.sql.Timestamp;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.application.crimsonfile.dao.UserDao;
import com.application.crimsonfile.entity.UserEntity;

/**
 * @author koki.esaki
 *
 */
@Service
public class LoginUserDetailsService implements UserDetailsService {

    @Autowired
    HttpSession session;

    @Autowired
    UserDao userDao;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        UserEntity user = userDao.findByEmail(email);
        session.setAttribute("user", user);
        if (user == null) {
            throw new UsernameNotFoundException("The requested user is not found");
        } else {
            // 前回ログイン時間を表示する場合はsave前にreturn
            Timestamp now = new Timestamp(System.currentTimeMillis());
            user.setPreviousLogin(now);
            userDao.save(user);
        }
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        /*
         * if(user.isAdmin==true) { authorities.add(new
         * SimpleGrantedAuthority("ROLE_ADMIN")); }
         */
        return new LoginUserDetails(user, authorities);
    }

}
