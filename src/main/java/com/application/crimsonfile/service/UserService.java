package com.application.crimsonfile.service;

import java.io.IOException;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.application.crimsonfile.dto.SignUpDto;
import com.application.crimsonfile.logic.UserLogic;

import lombok.NonNull;

/**
 * @author koki.esaki
 *
 */
@Service
public class UserService {

    @Autowired
    HttpSession session;

    @Autowired
    UserLogic userLogic;

    /**
     * ユーザーを新規会員登録
     *
     * @return
     * @throws IOException
     * @throws IllegalStateException
     */
    public void registUserInfo(@NonNull final SignUpDto dto) throws IllegalStateException, IOException {
        userLogic.saveUserInfo(dto);
    }

    /**
     * ユーザー情報を編集.
     *
     * @return
     * @throws IOException
     * @throws IllegalStateException
     */
    public void editUserInfo(@NonNull final SignUpDto dto) throws IllegalStateException, IOException {
        userLogic.editUserInfo(dto);
        System.out.println("ユーザー情報編集実行");
    }
}
