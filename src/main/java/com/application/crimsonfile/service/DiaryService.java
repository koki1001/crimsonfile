package com.application.crimsonfile.service;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.application.crimsonfile.dao.DiaryDao;
import com.application.crimsonfile.dto.DiaryDto;
import com.application.crimsonfile.dto.SearchDto;
import com.application.crimsonfile.entity.DiaryEntity;
import com.application.crimsonfile.entity.UserEntity;

import lombok.NonNull;

/**
 * @author koki.esaki
 *
 */
@Service
public class DiaryService {

    @Autowired
    DiaryDao diaryDao;

    @Autowired
    HttpSession session;

    /**
     * 日記登録.
     *
     * @return List<DiaryEntity>
     */
    public List<DiaryEntity> getAllDiary() {
        return diaryDao.findAll();
    }

    /**
     * 日記登録.
     *
     * @param dto
     * @param diaryId
     * @param favoriteFlag
     */
    public void saveDiary(@NonNull final DiaryDto dto, final Integer diaryId, final String favoriteFlag) {
        UserEntity userEntity = (UserEntity) session.getAttribute("user");
        DiaryEntity diaryEntity = new DiaryEntity();
        Date now = new Date();
        if (diaryId != null) {
            diaryEntity.setDiaryId(diaryId);
        }
        diaryEntity.setFavoriteFlag(favoriteFlag);
        diaryEntity.setUserId(userEntity.getId());
        diaryEntity.setTitle(dto.getTitle());
        diaryEntity.setContent(dto.getContent());
        diaryEntity.setDateTime(dto.getDate());
        diaryEntity.setCreateDateTime(now.toString());
        diaryDao.save(diaryEntity);
    }

    /**
     * 日記取得.
     *
     * @param offset
     * @param limit
     * @return List<DiaryEntity>
     */
    public List<DiaryEntity> getDiaryByUserId(int offset, int limit) {
        UserEntity userEntity = (UserEntity) session.getAttribute("user");
        return diaryDao.findByUserIdOrderByDateTimeDescCreateDateTimeDesc(userEntity.getId(), new PageRequest(offset, limit));
    }

    /**
     * 日記詳細取得.
     *
     * @param dto
     * @return DiaryEntity
     */
    public DiaryEntity getDiaryByDiaryId(DiaryDto dto) {
        return diaryDao.findByDiaryId(dto.getDiaryId());
    }

    /**
     * 検索結果の取得（日付）.
     *
     * @param dto
     * @return List<DiaryEntity>
     */
    public List<DiaryEntity> searchByDateTime(SearchDto dto) {
        return diaryDao.findByDateTimeOrderByDateTimeDescCreateDateTimeDesc(dto.getDate());
    }

    /**
     * 検索結果の取得（日記内容）.
     *
     * @param dto
     * @return List<DiaryEntity>
     */
    public List<DiaryEntity> searchByTitleOrContent(SearchDto dto) {
        return diaryDao.findByTitleOrContentContainingOrderByDateTimeDescCreateDateTimeDesc(dto.getContent(), dto.getContent());
    }

    /**
     * お気に入り設定する.
     *
     *　@param favoriteFlag
     * @param dto
     */
    public void favoriteDiaryByDiaryId(String favoriteFlag, DiaryDto dto) {
        diaryDao.saveByDiaryId(favoriteFlag, dto.getDiaryId());
    }

    /**
     * お気に入りの取得.
     *
     * @param dto
     * @return List<DiaryEntity>
     */
    public List<DiaryEntity> getFavoriteDiary() {
        return diaryDao.findByFavoriteFlagOrderByDateTimeDescCreateDateTimeDesc("1");
    }

    /**
     * 日記削除.
     *
     * @param dto
     */
    public void deleteByDiaryId(DiaryDto dto) {
        diaryDao.deleteByDiaryId(dto.getDiaryId());
    }

}
