package com.application.crimsonfile.service;

import java.util.List;

import org.springframework.security.core.GrantedAuthority;

import com.application.crimsonfile.entity.UserEntity;

/**
 * @author koki.esaki
 *
 */
public class LoginUserDetails extends org.springframework.security.core.userdetails.User {

    private static final long serialVersionUID = 1L;
    private final UserEntity user;

    public LoginUserDetails(UserEntity user, List<GrantedAuthority> authorities) {
        super(user.getEmail(), user.getPassword(), authorities);
        this.user = user;
    }

    public UserEntity getUser() {
        return user;
    }
}