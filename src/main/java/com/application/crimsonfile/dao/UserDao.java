package com.application.crimsonfile.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.application.crimsonfile.entity.UserEntity;

import lombok.NonNull;

/**
 * @author koki.esaki
 *
 */
public interface UserDao extends JpaRepository<UserEntity, String> {

	/**
	 * email検索.
	 *
	 * @param email
	 * @return UserEntity
	 */
	public UserEntity findByEmail(@NonNull final String email);
}
