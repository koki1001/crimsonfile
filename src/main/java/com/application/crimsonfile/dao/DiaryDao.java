package com.application.crimsonfile.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.application.crimsonfile.entity.DiaryEntity;

import lombok.NonNull;

/**
 * @author koki.esaki
 *
 */
@Transactional
@Repository
public interface DiaryDao extends JpaRepository<DiaryEntity, String> {

	/**
	 * diaryId検索.
	 *
	 * @param diaryId
	 * @return DiaryEntity
	 */
	public DiaryEntity findByDiaryId(@NonNull final int diaryId);

	/**
	 * userId検索.
	 *
	 * @param userId
	 * @return List<DiaryEntity>
	 */
	public List<DiaryEntity> findByUserIdOrderByDateTimeDescCreateDateTimeDesc(@NonNull final int userId, @NonNull final Pageable pageable);

	/**
	 * dateTime検索.
	 *
	 * @param dateTime
	 * @return List<DiaryEntity>
	 */
	public List<DiaryEntity> findByDateTimeOrderByDateTimeDescCreateDateTimeDesc(@NonNull final String dateTime);

	/**
	 * content検索.
	 *
	 * @param content
	 * @return List<DiaryEntity>
	 */
	public List<DiaryEntity> findByTitleOrContentContainingOrderByDateTimeDescCreateDateTimeDesc(@NonNull final String title, @NonNull final String content);

	/**
	 * favorite検索.
	 *
	 * @param favorite
	 * @return List<DiaryEntity>
	 */
	public List<DiaryEntity> findByFavoriteFlagOrderByDateTimeDescCreateDateTimeDesc(@NonNull final String favoriteFlag);

	/**
	 * favoriteFlag更新.
	 *
	 * @param favoriteFlag
	 */
	@Modifying
	@Query(value = "UPDATE DiaryEntity d SET d.favoriteFlag = :favoriteFlag WHERE d.diaryId = :diaryId")
	public void saveByDiaryId(@Param("favoriteFlag") @NonNull final String favoriteFlag, @Param("diaryId") @NonNull final int diaryId);

	/**
	 * diaryId削除.
	 *
	 * @param diaryId
	 */
	public void deleteByDiaryId(@NonNull final int diaryId);

}
