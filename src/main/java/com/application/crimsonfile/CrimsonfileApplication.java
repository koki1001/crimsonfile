package com.application.crimsonfile;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * @author koki.esaki
 *
 * 起動.
 */
@SpringBootApplication
public class CrimsonfileApplication {
    public static void main(String[] args) {
        SpringApplication.run(CrimsonfileApplication.class, args);
    }
}