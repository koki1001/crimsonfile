package com.application.crimsonfile.aspect;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class CrimsonfileAspect {

    @Before("execution(* jp.co.youthcat.crimsonfile.service.*.editUserInfo(..))")
    public void before() {
        System.out.println("ユーザー情報編集実行前");
    }

    @After("execution(* jp.co.youthcat.crimsonfile.service.*.editUserInfo(..))")
    public void after() {
        System.out.println("ユーザー情報編集実行");
    }
}
