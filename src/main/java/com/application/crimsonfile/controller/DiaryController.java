package com.application.crimsonfile.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.application.crimsonfile.constant.Constant;
import com.application.crimsonfile.dto.DiaryDto;
import com.application.crimsonfile.dto.SearchDto;
import com.application.crimsonfile.entity.DiaryEntity;
import com.application.crimsonfile.service.DiaryService;
import com.application.crimsonfile.util.DateUtil;

/**
 * @author koki.esaki
 *
 */
@Controller
@RequestMapping("/crimsonfile/diary")
public class DiaryController {

    @Autowired
    DiaryService diaryService;

    @ModelAttribute
    public DiaryDto setUpDiaryDto() {
        return new DiaryDto();
    }

    /**
     * 日記初期画面の表示.
     *
     * @return diary.jsp
     */
    @RequestMapping("/")
    public String diary(Model model) {
        List<DiaryEntity> diaryEntityList = diaryService.getDiaryByUserId(0, 10);
        model.addAttribute("diaryEntityList", diaryEntityList);
        model.addAttribute("pageNumber", "1");
        model.addAttribute("pageSum", ((diaryService.getAllDiary().size() - 1) / 10) + 1);
        model.addAttribute("date", DateUtil.getCurrentDateTime());
        return Constant.DIARY;
    }

    @RequestMapping("/page")
    public String diaryPaging(Model model, @RequestParam("num") Integer num) {
        List<DiaryEntity> diaryEntityList = diaryService.getDiaryByUserId(num - 1, 10);
        model.addAttribute("diaryEntityList", diaryEntityList);
        model.addAttribute("pageNumber", num);
        model.addAttribute("pageSum", (diaryService.getAllDiary().size() / 10) + 1);
        model.addAttribute("date", DateUtil.getCurrentDateTime());
        return Constant.DIARY;
    }

    /**
     * 日記登録画面の表示.
     *
     * @return diary_regist.jsp
     */
    @RequestMapping("/regist")
    public String registDiary(Model model) {
        model.addAttribute("date", DateUtil.getCurrentDateTime());
        return Constant.DIARY_REGIST;
    }

    /**
     * 日記登録後画面の表示.
     *
     * @return diary.jsp
     */
    @RequestMapping("/registcomplete")
    public String completeRegistDiary(DiaryDto dto, Model model) {
        diaryService.saveDiary(dto, null, "0");
        return diary(model);
    }

    /**
     * 日記編集画面の表示.
     *
     * @return diary_edit.jsp
     */
    @RequestMapping("/edit")
    public String editDiary(DiaryDto dto, Model model) {
        DiaryEntity diaryEntity = diaryService.getDiaryByDiaryId(dto);
        model.addAttribute("diaryEntity", diaryEntity);
        return Constant.DIARY_EDIT;
    }

    /**
     * 日記編集後画面の表示.
     *
     * @return diary.jsp
     */
    @RequestMapping("/editcomplete")
    public String completeEditDiary(DiaryDto dto, Model model) {
        DiaryEntity diaryEntity = diaryService.getDiaryByDiaryId(dto);
        diaryService.saveDiary(dto, dto.getDiaryId(), diaryEntity.getFavoriteFlag());
        return diary(model);
    }

    /**
     * 日記閲覧画面の表示.
     *
     * @return diary_view.jsp
     */
    @RequestMapping("/view")
    public String viewDiary(DiaryDto dto, Model model) {
        DiaryEntity diaryEntity = diaryService.getDiaryByDiaryId(dto);
        model.addAttribute("diaryEntity", diaryEntity);
        return Constant.DIARY_VIEW;
    }

    /**
     * 日記削除.
     *
     * @return diary.jsp
     */
    @RequestMapping("/delete")
    public String deleteDiary(DiaryDto dto, Model model) {
        diaryService.deleteByDiaryId(dto);
        return diary(model);
    }

    /**
     * お気に入り.
     *
     * @return diary.jsp
     */
    @RequestMapping("/favorite")
    public String favoriteDiary(DiaryDto dto, Model model) {
        diaryService.favoriteDiaryByDiaryId("1", dto);
        return diary(model);
    }

    /**
     * お気に入り削除.
     *
     * @return diary.jsp
     */
    @RequestMapping("/favoriteempty")
    public String favoriteEmptyDiary(DiaryDto dto, Model model) {
        diaryService.favoriteDiaryByDiaryId("0", dto);
        return diary(model);
    }

    /**
     * お気に入り表示.
     *
     * @return diary.jsp
     */
    @RequestMapping("/displayfavorite")
    public String getFavoriteDiary(Model model) {
        List<DiaryEntity> diaryEntityList = diaryService.getFavoriteDiary();
        model.addAttribute("diaryEntityList", diaryEntityList);
        model.addAttribute("pageNumber", "1");
        model.addAttribute("pageSum", (diaryService.getAllDiary().size() / 10) + 1);
        model.addAttribute("date", DateUtil.getCurrentDateTime());
        return Constant.DIARY;
    }

    /**
     * 日記検索結果画面（日付）の表示.
     *
     * @return diary.jsp
     */
    @RequestMapping("/searchByDate")
    public String searchByDate(SearchDto dto, Model model) {
        List<DiaryEntity> diaryEntityList = diaryService.searchByDateTime(dto);
        model.addAttribute("diaryEntityList", diaryEntityList);
        model.addAttribute("pageNumber", "1");
        model.addAttribute("pageSum", (diaryService.getAllDiary().size() / 10) + 1);
        model.addAttribute("date", dto.getDate());
        return Constant.DIARY;
    }

    /**
     * 日記検索結果画面（日記内容）の表示.
     *
     * @return diary.jsp
     */
    @RequestMapping("/searchByContent")
    public String searchByContent(SearchDto dto, Model model) {
        List<DiaryEntity> diaryEntityList = diaryService.searchByTitleOrContent(dto);
        model.addAttribute("diaryEntityList", diaryEntityList);
        model.addAttribute("pageNumber", "1");
        model.addAttribute("pageSum", (diaryService.getAllDiary().size() / 10) + 1);
        model.addAttribute("date", DateUtil.getCurrentDateTime());
        return Constant.DIARY;
    }

    /**
     * 日記検索結果画面（ページナンバー）の表示.
     *
     * @return diary.jsp
     */
    @RequestMapping("/searchByPage")
    public String searchByPage(SearchDto dto, Model model) {
        Integer page = dto.getPage();
        if (page == null) {
            return diary(model);
        }
        List<DiaryEntity> diaryEntityList = diaryService.getDiaryByUserId(page - 1, 10);
        model.addAttribute("diaryEntityList", diaryEntityList);
        model.addAttribute("pageNumber", dto.getPage());
        model.addAttribute("pageSum", (diaryService.getAllDiary().size() / 10) + 1);
        model.addAttribute("date", DateUtil.getCurrentDateTime());
        return Constant.DIARY;
    }
}
