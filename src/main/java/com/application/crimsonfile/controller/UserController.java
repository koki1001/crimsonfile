package com.application.crimsonfile.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.application.crimsonfile.constant.Constant;
import com.application.crimsonfile.dao.UserDao;
import com.application.crimsonfile.dto.SignUpDto;
import com.application.crimsonfile.entity.UserEntity;
import com.application.crimsonfile.service.UserService;

/**
 * @author koki.esaki
 *
 */
@RequestMapping("/user")
@Controller
public class UserController {

	@Autowired
	UserDao userDao;

	@Autowired
	UserService userService;

	@ModelAttribute
	public SignUpDto setUpSignUpForm() {
		return new SignUpDto();
	}

	/**
	 * エラー画面の表示.
	 *
	 * @return error.jsp
	 */
	@ExceptionHandler
	public String handleException(Exception e) {
		e.printStackTrace();
		return Constant.ERROR;
	}

	/**
	 * SignUpページに遷移.
	 *
	 * @return signUp.jsp
	 */
	@RequestMapping("/signup")
	public String signup() {
		return Constant.SIGN_UP;
	}

	/**
	 * Loginページに遷移.
	 *
	 * @return login.jsp
	 */
	@RequestMapping("/login")
	public String login() {
		return Constant.LOGIN;
	}

	/**
	 * ユーザーの新規登録.<br>
	 * 既に登録されている場合はregister画面のまま
	 *
	 * @param signUpDto
	 * @param model
	 * @throws IOException
	 * @throws IllegalStateException
	 */
	@RequestMapping("/register")
	public String register(SignUpDto signUpDto, Model model) throws IllegalStateException, IOException {
		// パスワードの整合性チェック
		if (!signUpDto.getPassword().equals(signUpDto.getConfirmationPassword())) {
			return Constant.SIGN_UP;
		}
		// メールアドレス重複チェック
		UserEntity user = userDao.findByEmail(signUpDto.getEmail());
		if (user == null) {
			// ユーザ情報の登録
			userService.registUserInfo(signUpDto);
			return Constant.LOGIN;
		}
		model.addAttribute("msg", "The email has already been registered");
		return Constant.SIGN_UP;
	}

	/**
	 * プロフィールの編集.
	 *
	 * @param signUpDto
	 * @param model
	 * @throws IOException
	 * @throws IllegalStateException
	 */
	@RequestMapping("/edit")
	public String edit(SignUpDto signUpDto, Model model) throws IllegalStateException, IOException {
		// ユーザ情報の編集
		userService.editUserInfo(signUpDto);

		return Constant.TOP;
	}
}
