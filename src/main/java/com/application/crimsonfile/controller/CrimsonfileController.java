package com.application.crimsonfile.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.application.crimsonfile.constant.Constant;

/**
 * @author koki.esaki
 *
 */
@Controller
@RequestMapping("/crimsonfile")
public class CrimsonfileController {

    /**
     * トップ画面の表示.
     *
     * @return index.jsp
     */
    @RequestMapping("/")
    public String index() {
        return Constant.TOP;
    }
}
