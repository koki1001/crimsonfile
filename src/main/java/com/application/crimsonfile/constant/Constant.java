package com.application.crimsonfile.constant;

/**
 * @author koki.esaki
 *
 */
public class Constant {

    /**ログイン画面.*/
    public static final String LOGIN = "user/login";

    /**アカウント作成画面.*/
    public static final String SIGN_UP = "user/signUp";

    /**トップ画面.*/
    public static final String TOP = "top/index";

    /**日記初期画面.*/
    public static final String DIARY = "top/diary/diary";

    /**日記登録画面.*/
    public static final String DIARY_REGIST = "top/diary/diary_regist";

    /**日記編集画面.*/
    public static final String DIARY_EDIT = "top/diary/diary_edit";

    /**日記閲覧画面.*/
    public static final String DIARY_VIEW = "top/diary/diary_view";

    /**エラー画面*/
    public static final String ERROR = "common/error";

    /**日付フォーマット*/
    public static final String DATE_FORMAT = "yyyy/MM/dd";
}
