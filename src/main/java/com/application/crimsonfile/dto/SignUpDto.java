package com.application.crimsonfile.dto;

import org.springframework.web.multipart.MultipartFile;

import lombok.Data;

/**
 * @author koki.esaki
 *
 */
@Data
public class SignUpDto {

    /**名前.*/
    private String name;

    /**性別.*/
    private String gender;

    /**配偶者の有無.*/
    private String maritalStatus;

    /**生年月日.*/
    private String year;

    private String month;

    private String day;

    /**血液型.*/
    private String bloodType;

    /**戸籍.*/
    private String familyRegister;

    /**メールアドレス.*/
    private String email;

    /**パスワード.*/
    private String password;

    /**確認用パスワード.*/
    private String confirmationPassword;

    /**プロフィール写真.*/
    private MultipartFile profilePhoto;
}
