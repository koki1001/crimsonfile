package com.application.crimsonfile.dto;

import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;

/**
 * @author koki.esaki
 *
 */
@Data
public class LoginDto {

    /**メールアドレス（ユーザID）.*/
    @NotBlank(message = "enter your email")
    private String email;

    /**パスワード.*/
    @NotBlank(message = "enter your password")
    private String password;
}
