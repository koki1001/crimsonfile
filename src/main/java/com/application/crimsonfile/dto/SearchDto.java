package com.application.crimsonfile.dto;

import lombok.Data;

/**
 * @author koki.esaki
 *
 */
@Data
public class SearchDto {

    /**日付.*/
    private String date;

    /**ページナンバー.*/
    private Integer page;

    /**日記内容.*/
    private String content;
}
