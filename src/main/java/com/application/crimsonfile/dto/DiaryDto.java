package com.application.crimsonfile.dto;

import lombok.Data;

/**
 * @author koki.esaki
 *
 */
@Data
public class DiaryDto {

	/**題名.*/
	private String title;
	/**内容.*/
	private String content;
	/**日記ID.*/
	private Integer diaryId;
	/**日付.*/
	private String date;
}
