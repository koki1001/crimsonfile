package com.application.crimsonfile.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.application.crimsonfile.constant.Constant;

public class DateUtil {

    public static String getCurrentDateTime() {
        final Date now = new Date();
        final SimpleDateFormat sdf = new SimpleDateFormat(Constant.DATE_FORMAT);
        return sdf.format(now);
    }
}
