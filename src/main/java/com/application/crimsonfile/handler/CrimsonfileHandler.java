package com.application.crimsonfile.handler;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.application.crimsonfile.constant.Constant;

@ControllerAdvice
public class CrimsonfileHandler {

    /**
     * エラー画面の表示.
     *
     * @return error.jsp
     */
    @ExceptionHandler(value = { Exception.class })
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public String handleException(Exception e) {
        e.printStackTrace();
        return Constant.ERROR;
    }
}
